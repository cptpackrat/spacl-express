
import * as express from 'express'
import * as supertest from 'supertest'
import { parse } from '@spacl/yaml'
import { checkPolicy, checkPolicies } from '../src'

const policies = parse(`
  version: 1.2
  policies:
    - name: user
      rules:
        - path: /user/+
          allow: GET
        - path: /user/:name
          allow: PUT
    - name: admin
      base: user
      rules:
        - path: /user/+
          allow:
            - PUT
            - DELETE
        - path: /user/:name
          deny: DELETE
`)

describe('checkPolicy', () => {
  const test = mkTest(checkPolicy(policies.get('user')!))
  it('accepts requests allowed by policy', () => test.get('/user/foo').expect(200))
  it('rejects requests denied by policy', () => test.get('/user/foo/bar').expect(403))
  context('when using \'getContext\'', () => {
    const test = mkTest(checkPolicy(policies.get('user')!, {
      getContext: () => ({ name: 'foo' })
    }))
    it('accepts requests allowed by policy', () => test.put('/user/foo').expect(200))
    it('rejects requests denied by policy', () => test.put('/user/bar').expect(403))
  })
  context('when using \'onAllow\'', () => {
    const test = mkTest(checkPolicy(policies.get('user')!, {
      onAllow: (_req, res) => res.sendStatus(204)
    }))
    it('accepts requests allowed by policy', () => test.get('/user/foo').expect(204))
    it('rejects requests denied by policy', () => test.put('/user/foo').expect(403))
  })
  context('when using \'onDeny\'', () => {
    const test = mkTest(checkPolicy(policies.get('user')!, {
      onDeny: (_req, res) => res.sendStatus(401)
    }))
    it('accepts requests allowed by policy', () => test.get('/user/foo').expect(200))
    it('rejects requests denied by policy', () => test.put('/user/foo').expect(401))
  })
})

describe('checkPolicies', () => {
  const test = mkTest(checkPolicies(policies, {
    getPolicy: (req) => req.query.policy
  }))
  it('accepts requests allowed by policies', () => test.put('/user/foo?policy=admin').expect(200))
  it('rejects requests denied by policies', () => test.put('/user/foo?policy=user').expect(403))
  context('when using \'getContext\'', () => {
    const test = mkTest(checkPolicies(policies, {
      getPolicy: (req) => req.query.policy,
      getContext: () => ({ name: 'foo' })
    }))
    it('accepts requests allowed by policies', () => test.put('/user/foo?policy=user').expect(200))
    it('rejects requests denied by policies', () => test.put('/user/bar?policy=user').expect(403))
  })
  context('when using \'onAllow\'', () => {
    const test = mkTest(checkPolicies(policies, {
      getPolicy: (req) => req.query.policy,
      onAllow: (_req, res) => res.sendStatus(204)
    }))
    it('accepts requests allowed by policies', () => test.put('/user/foo?policy=admin').expect(204))
    it('rejects requests denied by policies', () => test.put('/user/foo?policy=user').expect(403))
  })
  context('when using \'onDeny\'', () => {
    const test = mkTest(checkPolicies(policies, {
      getPolicy: (req) => req.query.policy,
      onDeny: (_req, res) => res.sendStatus(401)
    }))
    it('accepts requests allowed by policies', () => test.put('/user/foo?policy=admin').expect(200))
    it('rejects requests denied by policies', () => test.put('/user/foo?policy=user').expect(401))
  })
})

function mkTest (handler: express.RequestHandler) {
  return supertest(express()
    .use(handler)
    .use((_req, res) => res.sendStatus(200)))
}
