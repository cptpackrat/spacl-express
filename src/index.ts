
import { Queryable, QueryableMap, QueryContext } from '@spacl/core'
import { RequestHandler, Request, Response, NextFunction } from 'express'

export interface CheckPolicyOpts {
  /** Optional callback function to collect query context; if ommitted, no
    * context will be passed to queries. */
  getContext?: (req: Request, res: Response) => QueryContext
  /** Optional callback function to handle allowed requests; if ommitted, a
    * default implementation that calls `next()` is used. */
  onAllow?: (req: Request, res: Response, next: NextFunction) => void
  /** Optional callback function to handle denied requests; if ommitted, a
    * default implementation that calls `res.sendStatus(403)` is used. */
  onDeny?: (req: Request, res: Response, next: NextFunction) => void
}

export interface CheckPoliciesOpts extends CheckPolicyOpts {
  /** Callback function to retrieve the name of the policy that should
    * be queried for this request. */
  getPolicy: (req: Request, res: Response) => string
}

/** Returns a middleware function that validates requests against a single policy.
  * @param policy Access control policy.
  * @param opts Additional options. */
export function checkPolicy (policy: Queryable, opts: CheckPolicyOpts = {}): RequestHandler {
  const {
    getContext,
    onAllow,
    onDeny
  } = Object.assign({
    onAllow: (_req: Request, _res: Response, next: NextFunction) => next(),
    onDeny: (_req: Request, res: Response) => res.sendStatus(403)
  }, opts)
  return (req, res, next) => {
    const ctx = getContext && getContext(req, res)
    return policy.query(req.path, req.method, ctx)
      ? onAllow(req, res, next)
      : onDeny(req, res, next)
  }
}

/** Returns a middleware function that validates requests against a collection of policies.
  * @param policies Access control policies.
  * @param opts Additional options. */
export function checkPolicies (policies: QueryableMap, opts: CheckPoliciesOpts): RequestHandler {
  const {
    getPolicy,
    getContext,
    onAllow,
    onDeny
  } = Object.assign({
    onAllow: (_req: Request, _res: Response, next: NextFunction) => next(),
    onDeny: (_req: Request, res: Response) => res.sendStatus(403)
  }, opts)
  return (req, res, next) => {
    const ctx = getContext && getContext(req, res)
    return policies.query(getPolicy(req, res), req.path, req.method, ctx)
      ? onAllow(req, res, next)
      : onDeny(req, res, next)
  }
}
