@spacl/express
======
[![npm version][npm-image]][npm-url]
[![pipeline status][pipeline-image]][pipeline-url]
[![coverage status][coverage-image]][coverage-url]
[![standard-js][standard-image]][standard-url]

Express middleware for [SPACL](https://www.npmjs.com/package/@spacl/core) policies.

## Installation
```
npm install @spacl/express
```
## Documentation
API documentation is available [here](https://cptpackrat.gitlab.io/spacl-express).

## Example
First we need to create a set of policies:
```yaml
# example.yml
version: 1.2
policies:
  # Create a policy describing a standard user who can
  # view other user's profiles, and edit their own.
  - name: user
    rules:
      - path: /user/+
        allow: GET
      - path: /user/:name
        allow: PUT
  # Create a derived policy describing an admin user who
  # can view, edit and delete other users' profiles, but
  # cannot delete themselves.
  - name: admin
    base: user
    rules:
      - path: /user/+
        allow:
          - PUT
          - DELETE
      - path: /user/:name
        deny: DELETE
```
Now we can create a basic express application that uses these policies to govern access to protected
resources:
```js
import * as express from 'express'
import { parseFileSync } from '@spacl/yaml'
import { checkPolicies } from '@spacl/express'

const app = express()
  .use(/* Your authentication middleware here. */)
  .use(checkPolicies(parseFileSync('example.yml'), {
    /* We need to know which policy to apply for this request; the authentication
       middleware above would typically be responsible for providing this. */
    getPolicy: (req, res) => req.user.policy
  }))
  /* All routes below this line are now guarded by access control policy. */
  .use('/user', /* Your route handlers here. */)
  .use(...)
  .use(...)
```

[npm-image]: https://img.shields.io/npm/v/@spacl/express.svg
[npm-url]: https://www.npmjs.com/package/@spacl/express
[pipeline-image]: https://gitlab.com/cptpackrat/spacl-express/badges/master/pipeline.svg
[pipeline-url]: https://gitlab.com/cptpackrat/spacl-express/commits/master
[coverage-image]: https://gitlab.com/cptpackrat/spacl-express/badges/master/coverage.svg
[coverage-url]: https://gitlab.com/cptpackrat/spacl-express/commits/master
[standard-image]: https://img.shields.io/badge/code%20style-standard-brightgreen.svg
[standard-url]: http://standardjs.com/
